<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Web'], function() {
    Route::get('/', 'HomeController@index')->name('home');
});

Route::namespace('Admin')->name('admin.')->prefix('admin')->group(function() {
    Route::get('/agents', 'AgentController@index')->name('agent.index');
    Route::get('/agent/create', 'AgentController@create')->name('agent.create');
    Route::post('/agent/store', 'AgentController@store')->name('agent.store');
    Route::get('/agent/show/{agent}', 'AgentController@show')->name('agent.show');
    Route::get('/agent/edit/{agent}', 'AgentController@edit')->name('agent.edit');
    Route::put('/agent/update/{agent}', 'AgentController@update')->name('agent.update');
    Route::delete('/agent/delete/{agent}', 'AgentController@destroy')->name('agent.delete');
});
