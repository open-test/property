<?php

Use App\Models\Agent;
Use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('API/V1')->name('api.')->group(function() {
    // Property
    Route::get('properties', function() {
        return Property::with('property_type')->get();
    })->name('property.index');

    Route::get('properties/{id}', function($id) {
        return Property::findOrFail($id);
    })->name('property.show');

    Route::post('properties', function(Request $request) {
        return Property::create($request->all);
    })->name('property.store');

    Route::put('properties/{id}', function(Request $request, $id) {
        $property = Property::findOrFail($id);
        $property->update($request->all());

        return $property;
    })->name('property.update');

    Route::delete('properties/{id}', function($id) {
        Property::find($id)->delete();
        return 204;
    })->name('property.delete');

    // Agent
    Route::get('agents', function() {
        return Agent::with('properties')->withCount('properties')->get()->append('total_price');
    })->name('agent.index');

    Route::get('agents/top', function() {
        return Agent::with('properties')->withCount('properties')->orderBy('properties_count', 'DESC')->get()->append('total_price');
    })->name('agent.top');

});
