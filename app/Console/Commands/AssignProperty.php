<?php

namespace App\Console\Commands;

use App\Models\Agent;
use App\Models\Property;
use Illuminate\Console\Command;

class AssignProperty extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assign:property';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $agents = Agent::get();

        foreach ($agents as $key => $agent) {
            $randomInt = rand(3,4);

            $properties = Property::get()->random($randomInt)->pluck('id');

            $agent->properties()->sync($properties);
        }

        $this->info('Agents assigned properties!');
    }
}
