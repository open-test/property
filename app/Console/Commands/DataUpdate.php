<?php

namespace App\Console\Commands;

use App\Models\Property;
use App\Models\PropertyType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class DataUpdate extends Command
{
    const BASE_URL = 'https://trial.craig.mtcserver15.com';

    const PROPERTIES_URL = '/api/properties';

    private $apiKey;
    private $pageNumber;
    private $pageSize;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update property data via. the API if change made.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->apiKey = env('TRIAL_API_KEY');
        $this->pageNumber = 30;
        $this->pageSize = 30;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $response = self::getRequest();

        $data = $response['data'];

        self::update($data);

    }

    public function getRequest()
    {
        $url = self::BASE_URL . self::PROPERTIES_URL;

        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $url, [
            'query' => [
               'api_key' => $this->apiKey,
               'page[number]' => $this->pageNumber,
               'page[size]' => $this->pageSize,
            ],
        ])->getBody()->getContents();

        $content = json_decode($response, true);

        return $content;
    }

    public function update($data)
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                // Check for update
                $property = Property::where('updated_at', '!=', $value['updated_at'])->find($value['uuid']);
                // If exist
                if ($property) {
                    // Property
                    $property->id = $value['uuid'];
                    $property->property_type_id = $value['property_type_id'];
                    $property->county = $value['county'];
                    $property->country = $value['country'];
                    $property->town = $value['town'];
                    $property->description = $value['description'];
                    $property->address = $value['address'];
                    $property->image_full = $value['image_full'];
                    $property->image_thumbnail = $value['image_thumbnail'];
                    $property->latitude = $value['latitude'];
                    $property->longitude = $value['longitude'];
                    $property->num_bedrooms = $value['num_bedrooms'];
                    $property->num_bathrooms = $value['num_bathrooms'];
                    $property->price = $value['price'];
                    $property->type = $value['type'];
                    $property->created_at = $value['created_at'];
                    $property->updated_at = $value['updated_at'];
                    $property->update();
                }


            }

            $this->info('Properties updated!');

        } else {

            $this->info('No property data found!');

        }
    }
}
