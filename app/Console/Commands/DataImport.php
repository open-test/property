<?php

namespace App\Console\Commands;

use App\Models\Property;
use App\Models\PropertyType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class DataImport extends Command
{
    const BASE_URL = 'https://trial.craig.mtcserver15.com';

    const PROPERTIES_URL = '/api/properties';

    private $apiKey;
    private $pageNumber;
    private $pageSize;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import property data via. the API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->apiKey = env('TRIAL_API_KEY');
        $this->pageNumber = 30;
        $this->pageSize = 30;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $response = self::getRequest();

        $properties = $response['data'];

        self::import($properties);

    }

    public function getRequest()
    {
        $url = self::BASE_URL . self::PROPERTIES_URL;

        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $url, [
            'query' => [
               'api_key' => $this->apiKey,
               'page[number]' => $this->pageNumber,
               'page[size]' => $this->pageSize,
            ],
        ])->getBody()->getContents();

        $content = json_decode($response, true);

        return $content;
    }

    public function import($properties)
    {

        if (!empty($properties)) {
            foreach ($properties as $key => $value) {
                // Property type
                $propertyType = PropertyType::firstOrCreate(
                    ['id' => $value['property_type']['id']],
                    ['title' => $value['property_type']['title']],
                    ['description' => $value['property_type']['description']],
                    ['created_at' => $value['property_type']['created_at']],
                    ['updated_at' => $value['property_type']['updated_at']]
                );
            }

            $this->info('Imported: Property Type imported!');

            foreach ($properties as $key => $value) {
                // Property
                $property = Property::firstOrNew(['id' => $value['uuid']]);
                $property->id = $value['uuid'];
                $property->property_type_id = $value['property_type_id'];
                $property->county = $value['county'];
                $property->country = $value['country'];
                $property->town = $value['town'];
                $property->description = $value['description'];
                $property->address = $value['address'];
                $property->image_full = $value['image_full'];
                $property->image_thumbnail = $value['image_thumbnail'];
                $property->latitude = $value['latitude'];
                $property->longitude = $value['longitude'];
                $property->num_bedrooms = $value['num_bedrooms'];
                $property->num_bathrooms = $value['num_bathrooms'];
                $property->price = $value['price'];
                $property->type = $value['type'];
                $property->created_at = $value['created_at'];
                $property->updated_at = $value['updated_at'];
                $property->save();
            }

            $this->info('Imported: Properties updated!');

        } else {

            $this->info('No import data found!');

        }

    }
}
