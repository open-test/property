<?php

namespace App\Http\Controllers\Web;

use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
     /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('web.index');
    }

}
