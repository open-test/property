<?php

namespace App\Http\Controllers\Admin;

use App\Models\Agent;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AgentController extends Controller
{

    public function show($id)
    {
        return Agent::find($id);
    }

    public function create()
    {
        $properties = Property::get()->pluck('country', 'id');
        return view('admin.agents.create',compact('properties'));
    }


    public function store(Request $request)
    {
        // Add validation rules
        $validated = $request->validate([
            'full_name' => 'required|max:255',
            'email' => 'required|unique:agents',
            'phone' => 'required|max:15',
            'mobile' => 'required|max:15',
            'address' => 'required',
        ]);

        $properties = $request->input('properties');
        $agent = Agent::create($request->except('properties'));
        $agent->properties()->sync($properties);

        return redirect()->route('agent.index');
    }

    public function edit($id)
    {
        $agent = Agent::findOrFail($id);
        $properties = Property::get()->pluck('country', 'id');

        return view('admin.agents.create',compact('agent', 'properties'));
    }

    public function update(Request $request, $id)
    {
        $agent = Agent::findOrFail($id);
        $agent->update($request->all());

        return $agent;
    }

    public function destroy(Request $request, $id)
    {
        $agent = Agent::findOrFail($id);
        $agent->delete();

        return redirect()->route('agent.index');
    }
}
