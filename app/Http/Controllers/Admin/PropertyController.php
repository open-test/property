<?php

namespace App\Http\Controllers\Admin;

use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PropertyController extends Controller
{
    public function index()
    {
        return Property::all();
    }

    public function show($id)
    {
        return Property::find($id);
    }

    public function store(Request $request)
    {
        return Property::create($request->all());
    }

    public function edit($id)
    {
        return Property::find($id);
    }

    public function update(Request $request, $id)
    {
        $property = Property::findOrFail($id);
        $property->update($request->all());

        return $property;
    }

    public function destroy(Request $request, $id)
    {
        $property = Property::findOrFail($id);
        $property->delete();

        return 204;
    }

}
