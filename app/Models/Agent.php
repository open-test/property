<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    use HasFactory;

    protected $fillable = [
        'full_name',
        'email',
        'phone',
        'mobile',
        'address',
    ];


    public function properties()
    {
        return $this->belongsToMany(Property::class, 'agent_property', 'agent_id', 'property_id');
    }

    public function getTotalPriceAttribute()
    {

        $data = $this->properties->sum(function($query) {
             return $query->price;
        });

        return $data;
    }

}
