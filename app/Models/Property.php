<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'id',
        'property_type_id',
        'image_full',
        'image_thumbnail',
        'county',
        'country',
        'town',
        'address',
        'num_bedrooms',
        'num_bathrooms',
        'description',
        'type',
        'price',
        'latitude',
        'longitude',
    ];

    protected $appends = ['url'];

    public function property_type()
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    public function agents()
    {
        return $this->belongsToMany(Agent::class, 'agent_property', 'property_id', 'agent_id');
    }

    public function getUrlAttribute()
    {
        return route('api.property.show', [$this->id]);
    }

}
