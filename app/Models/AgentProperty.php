<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentProperty extends Model
{

    protected $table = "agent_property";

    protected $fillable = [
        'agent_id',
        'property_id',
    ];
}
