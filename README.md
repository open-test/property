

## About Propetry

A SnappyShopper dev test attempted by Paschal with Laravel 8 and Bootstrap 5.0.

# How to Setup

- Run >> composer install
- Run key gen command >> php artisan key:generate
- Run migration command >> php artisan migrate
- Run data import command to import all property types and properties >>  php artisan data:import
- Run command to create and assign agent to properties at random >>  php artisan db:seed
- Run command to assign 3 or 4 properties at random to an agent >>  php artisan assign:property
- Visit home page and follow links
- Run data update commad to update all properties if updated >> php artisan data:update


