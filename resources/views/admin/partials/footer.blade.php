<footer class="mt-5">
    <div class="container">
        <div class="col-12">
            <p class="text-muted">Copyright {{ date('Y') }} - CoronaStreet</p>
        </div>
    </div>
</footer>
