@extends('admin.layouts.master')

@section('title', 'Add Agent')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4>Add agent</h4>
                <form method="POST" action="{{route('admin.agent.store')}}">
                    @csrf
                    <div class="form-group">
                        <label for="full_name">Full name</label>
                        <input type="text" name="full_name" class="form-control" id="full_name" value="{{ old('full_name', $agent->full_name ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" id="email" value="{{ old('email', $agent->email ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone', $agent->phone ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="mobile">Mobile</label>
                        <input type="text" name="mobile" class="form-control" id="mobile" value="{{ old('mobile', $agent->mobile ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" class="form-control" id="address" value="{{ old('address', $agent->address ?? '') }}">
                    </div>

                    @if($properties)
                    <div class="form-group">
                        <label for="address">Properties</label>
                        <select class="form-control" name="properties" multiple>
                            @foreach($properties as $key => $property)
                            <option value="{{$key}}">{{$property}}</option>
                            @endforeach
                        </select>
                    </div>
                    @else
                    <div class="alert alert-info">To import properties run <code>php artisan import:data</code></div>
                    @endif
                    <div class="mt-3">
                        @if(empty($agent))
                        <button type="submit" class="btn btn-primary btn-block">Add agent</button>
                        @else
                        <button type="submit" class="btn btn-primary btn-block">Update agent</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop


