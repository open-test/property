@extends('web.layouts.master')

@section('title', 'Home')


@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center mb-5">Home</h1>
            </div>
            <div class="col-md-6">
                <div class="card mb-5">
                    <div class="card-body">
                        <h4>Add Agent</h4>
                        <p>Add:: agents to our database</p>
                        <a href="{{route('admin.agent.create')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-5">
                    <div class="card-body">
                        <h4>Properties</h4>
                        <p>View:: all properties in our database
                        <a href="{{route('api.property.index')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-5">
                    <div class="card-body">
                        <h4>Agents</h4>
                        <p>View:: all agents in our database
                        <a href="{{route('api.agent.index')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-5">
                    <div class="card-body">
                        <h4>Top Agents</h4>
                        <p>View:: Top agents in our database
                        <a href="{{route('api.agent.top')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
